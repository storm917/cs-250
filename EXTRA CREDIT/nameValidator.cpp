#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <vector>

using namespace std;

bool Validate(string);

int main()
{
	ofstream outFile;
	outFile.open("Names.txt");

	int choice = 0;
	string name;
	bool quit = false;

	while (!quit)
	{
		cout << "MAIN MENU" << endl << endl;
		cout << "1. Enter name" << endl
			<< "2. Quit" << endl
			<< "Choice: ";
		cin >> choice;

		if (choice == 1)
		{
			cout << "Enter a name: ";
			cin >> name;

			if (Validate(name))
			{
				outFile << name << endl;
			}
			else
			{
				cout << "Please enter a valid name" << endl;
			}
		}
		else if (choice == 2)
		{
			quit = true;
			cout << "Goodbye! ^_^";
		}
		else
			cout << "Please enter 1 or 2" << endl;
	}

	outFile.close();


	cin.get();
	cin.ignore();
	return 0;
}

bool Validate(string name)
{
	for (int i = 0; i < name.length(); i++)
	{
		if (isalpha(name[i]))
			return true;
		else if (name == " ")
			return true;
		else 
			return false;
	}
}