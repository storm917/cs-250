// Lab - Standard Template Library - Part 2 - Lists
// ZACH Davis

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states);

int main()
{
	list<string> states;

	bool done = false;
	while (!done)
	{
		cout << "MAIN MENU" << endl;
		cout << "1. Add to front \n"
			<< "2. Add to back \n"
			<< "3. Remove from front \n"
			<< "4. Remove from back \n"
			<< "5. Continue" << endl;

		int choice;
		cin >> choice;
		
		if (choice == 1) {
			string newState;
			cout << "State name: ";
			cin >> newState;
			states.push_front(newState);
		}
		else if (choice == 2) {
			string newState;
			cout << "State name: ";
			cin >> newState;
			states.push_back(newState);
		}
		else if (choice == 3) {
			states.pop_front();
		}
		else if (choice == 4) {
			states.pop_back();
		}
		else if (choice == 5) {
			cout << "Standard List" << endl;
			DisplayList(states); // displays states in order as entered
			cout << endl;
			cout << "Reversed List" << endl;
			states.reverse();
			DisplayList(states); // displays states as entered but reversed
			cout << endl;
			cout << "Sorted List" << endl;
			states.sort();
			DisplayList(states); // displays states sorted alphabetically
			cout << endl;
			cout << "Reversed Sorted List" << endl;
			states.reverse();
			DisplayList(states); // displays states sorted alphabetically but reversed
			
			done = true;
		}
	}

    cin.ignore();
    cin.get();
    return 0;
}
void DisplayList(list<string>& states)
{
	for (list<string>::iterator it = states.begin();
		it != states.end();
		it++)
	{
		cout << *it << "\t";
	}
	cout << endl;
}
