// Lab - Standard Template Library - Part 5 - Maps
// Zach Davis

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	map<char, string> colors;
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";

	cout << "Please enter one of the following: (r, g, b, c, m, y) or q to quit." << endl;
	cout << "Choice: ";

	bool done = false;
	while (!done)
	{
		char letter;
		cin >> letter;

		if (letter == 'q')
		{
			done = true;
			cout << "Press any key to exit.";
		}
		else
			cout << "Hex: " << colors[letter] << endl;
	}

    cin.ignore();
    cin.get();
    return 0;
}
