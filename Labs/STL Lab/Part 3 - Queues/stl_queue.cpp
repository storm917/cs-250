// Lab - Standard Template Library - Part 3 - Queues
// ZACH DAVIS

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;

	bool done = false;
	while (!done)
	{
		cout << "MAIN MENU" << endl;
		cout << "1. Add new transaction \n"
			<< "2. Continue \n"
			<< endl;

		int choice;
		cin >> choice;

		if (choice == 1) {
			float newTransaction;
			cout << "Enter transaction amount: ";
			cin >> newTransaction;
			transactions.push(newTransaction);
		}
		else if (choice == 2) {
			if (transactions.empty())
				cout << "Queue is empty" << endl;
			float balance = 0.0;
			while (!transactions.empty())
			{
				cout << "Transaction: $" << transactions.front() << endl;
				balance += transactions.front();
				transactions.pop();
			}
			cout << "Balance: $" << balance << endl;

			done = true;
		}
	}

    cin.ignore();
    cin.get();
    return 0;
}
