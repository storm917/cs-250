// Lab - Standard Template Library - Part 4 - Stacks
// ZACH DAVIS

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> letters;

	cout << "Enter the letters of a word, type UNDO to undo last letter, or DONE when finished" << endl;

	bool done = false;
	while (!done)
	{
		string letter;
		cout << "Input: ";
		cin >> letter;

		if (letter == "UNDO")
		{
			letters.pop();
		}
		else if (letter == "DONE")

		{
			done = true;
		}
		else
			letters.push(letter);

	}

	while (!letters.empty())
	{
		cout << letters.top() << endl;
		letters.pop();
	}

    cin.ignore();
    cin.get();
    return 0;
}
