#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{

	ofstream output(logFile);
	output << "First Come, First Served" << endl;

	int cycles = 0;
	int completionTime = 0;

	while (jobQueue.Size() > 0)
	{
		output << "Cycle: " << cycles << endl;
		output << "Processing job # " << jobQueue.Front()->id << endl;
		output << "Time remaining " << jobQueue.Front()->fcfs_timeRemaining << endl;
		jobQueue.Front()->Work(FCFS);

		if (jobQueue.Front()->fcfs_done)
		{
			jobQueue.Front()->fcfs_finishTime = cycles;
			jobQueue.Pop();
		}

		cycles++;
	}

	output << "Summary" << endl << endl;
	for (int i = 0; i < allJobs.size(); i++)
	{
		output << "Job ID: " << allJobs[i].id << endl;
		output << "Time to complete: " << allJobs[i].fcfs_finishTime << endl << endl;
		completionTime = completionTime + allJobs[i].fcfs_finishTime;
	}
	double average = completionTime / allJobs.size();
	output << "Average completion time " << average << endl;
	output << "Total processing time " << completionTime << endl;


	output.close();

}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin" << endl;

	int cycles = 0;
	int timer = 0;

	while (jobQueue.Size() > 0)
	{
		output << "Cycle: " << cycles << endl;
		output << "Processing job # " << jobQueue.Front()->id << endl;
		output << "Time remaining " << jobQueue.Front()->rr_timeRemaining << endl;
		output << "Times interrupted " << jobQueue.Front()->rr_timesInterrupted << endl;

		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;
		}

		jobQueue.Front()->Work(RR);

		if (jobQueue.Front()->rr_done)
		{
			jobQueue.Front()->SetFinishTime(timer, RR);
			jobQueue.Pop();
		}
		cycles++;
		timer++;
	}

	int completionTime = 0;
	output << "Summary" << endl << endl;
	for (int i = 0; i < allJobs.size(); i++)
	{
		completionTime = completionTime + allJobs[i].rr_finishTime;
		output << "Job ID: " << allJobs[i].id << endl;
		output << "Time to complete: " << allJobs[i].rr_finishTime << endl;
		output << "Times interrupted: " << allJobs[i].rr_timesInterrupted << endl;
		
	}
	double average = completionTime / allJobs.size();
	output << "Average completion time " << average << endl;
	output << "Total processing time " << completionTime << endl;
	output << "Round robin interval " << cycles << endl;
}

#endif
