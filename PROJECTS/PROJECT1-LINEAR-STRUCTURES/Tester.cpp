#include "Tester.hpp"

void Tester::RunTests()
{
	Test_IsEmpty();
	Test_IsFull();
	Test_Size();
	Test_GetCountOf();
	Test_Contains();

	Test_PushFront();
	Test_PushBack();

	Test_Get();
	Test_GetFront();
	Test_GetBack();

	Test_PopFront();
	Test_PopBack();
	Test_Clear();

	Test_ShiftRight();
	Test_ShiftLeft();

	Test_RemoveItem();
	Test_Insert();
}

void Tester::DrawLine()
{
	cout << endl;
	for (int i = 0; i < 80; i++)
	{
		cout << "-";
	}
	cout << endl;
}

void Tester::Test_Init()
{
	DrawLine();
	cout << "TEST: Test_Init" << endl;

	// Put tests here
}

void Tester::Test_ShiftRight()
{
	DrawLine();
	cout << "TEST: Test_ShiftRight" << endl;

	{
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);
		testList.ShiftRight(0);

		int expected = 3;
		int* actual = testList.Get(1);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;

		if (expected == *actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - not within index" << endl << endl;
		List<int> testList;

		bool expected = false;
		bool actual = testList.ShiftRight(3);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

}

void Tester::Test_ShiftLeft()
{
	DrawLine();
	cout << "TEST: Test_ShiftLeft" << endl;

	{
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);
		testList.RemoveIndex(1);
		testList.ShiftLeft(1);

		int expected = 1;
		int* actual = testList.Get(1);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;

		if (expected == *actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - not within index" << endl << endl;
		List<int> testList;

		bool expected = false;
		bool actual = testList.ShiftLeft(3);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}


}

void Tester::Test_Size()
{
	DrawLine();
	cout << "TEST: Test_Size" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		testList.PushBack(1);

		int expectedSize = 1;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_IsEmpty()
{
	DrawLine();
	cout << "TEST: Test_IsEmpty" << endl;

	{
		cout << "Test 1" << endl;
		List<int> testList;

		bool expectedOutput = true;
		bool actualOutput = testList.IsEmpty();

		cout << "Expected output: " << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;
		if (expectedOutput == actualOutput)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2" << endl;
		List<int> testList;
		testList.PushBack(1);

		bool expectedOutput = false;
		bool actualOutput = testList.IsEmpty();

		cout << "Expected output: " << expectedOutput << endl;
		cout << "Actual output: " << actualOutput << endl;
		if (expectedOutput == actualOutput)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}
}

void Tester::Test_IsFull()
{
	DrawLine();
	cout << "TEST: Test_IsFull" << endl;

	{
		cout << "Test 1" << endl << endl;
		List<int> testList;
		bool expectedOutput = false;
		bool actualOutput = testList.IsFull();
		cout << "Expected output: " << expectedOutput << endl
			<< "Actual output: " << actualOutput << endl;
		if (expectedOutput == actualOutput)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2" << endl << endl;
		List<int> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushFront(i);
		}
		bool expected = true;
		bool actual = testList.IsFull();
		cout << "Expected output: " << expected << endl
			<< "Acutual output: " << actual << endl;
		if (expected = actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}
}

void Tester::Test_PushFront()
{
	DrawLine();
	cout << "TEST: Test_PushFront" << endl;

	{
		cout << endl << "Test 1" << endl << endl;
		List<int> testList;
		bool expected = true;
		bool actual = testList.PushFront(1);
		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;
		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2" << endl << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);

		int expected = 3;
		int* actual = testList.Get(0);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;
		if (expected == *actual)
			cout << "PASS" << endl;
		else if (actual == nullptr)
			cout << "FAIL - nullptr" << endl;
		else if (expected != *actual)
			cout << "FAIL" << endl;


	}
}

void Tester::Test_PushBack()
{
	DrawLine();
	cout << "TEST: Test_PushBack" << endl;

	{
		cout << endl << "Test 1 - PushBack 3 items" << endl << endl;
		List<int> testList;
		testList.PushBack(1);
		testList.PushBack(2);
		testList.PushBack(3);

		int expected = 1;
		int* actual = testList.Get(0);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;
		if (actual == nullptr)
			cout << "FAIL - nullptr" << endl;
		else if (expected == *actual)
			cout << "PASS" << endl;
		else if (expected != *actual)
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - full list" << endl << endl;
		List<int> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushBack(i);
		}

		bool expected = false;
		bool actual = testList.PushBack(3);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;
		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL - bad return" << endl;
	}
}

void Tester::Test_PopFront()
{
	DrawLine();
	cout << "TEST: Test_PopFront" << endl;

	{
		cout << endl << "Test 1 - empty list" << endl << endl;
		List<int> testList;

		bool expected = false;
		bool actual = testList.PopFront();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;
		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - add one item then pop front" << endl << endl;
		List<int> testList;

		testList.PushFront(6);
		testList.PopFront();

		bool expected = true;
		bool actual = testList.IsEmpty();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;
		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}
}

void Tester::Test_PopBack()
{
	DrawLine();
	cout << "TEST: Test_PopBack" << endl;

	{
		cout << endl << "Test 1 - empty list" << endl << endl;
		List<int> testList;

		bool expected = false;
		bool actual = testList.PopBack();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;
		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - add one item then pop back" << endl << endl;
		List<int> testList;

		testList.PushFront(6);
		testList.PopBack();

		bool expected = true;
		bool actual = testList.IsEmpty();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;
		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

}

void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_Clear" << endl;

	{
		cout << endl << "Test 1" << endl << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);
		testList.Clear();

		bool expected = true;
		bool actual = testList.IsEmpty();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

}

void Tester::Test_Get()
{
	DrawLine();
	cout << "TEST: Test_Get" << endl;

	{
		cout << endl << "Test 1 - empty list" << endl << endl;
		List<int> testList;

		if (testList.Get(0) == nullptr)
			cout << "PASS - returned nullptr" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - add a value" << endl << endl;
		List<int> testList;

		testList.PushFront(1);

		int expected = 1;
		int* actual = testList.Get(0);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;

		if (actual == nullptr)
			cout << "FAIL - returned nullptr" << endl;
		else if (expected == *actual)
			cout << "PASS - same value" << endl;
		else if (expected != *actual)
			cout << "FAIL - different values" << endl;
	}
}

void Tester::Test_GetFront()
{
	DrawLine();
	cout << "TEST: Test_GetFront" << endl;

	{
		cout << endl << "Test 1 - add one item" << endl << endl;
		List<int> testList;

		testList.PushFront(1);

		int expected = 1;
		int* actual = testList.GetFront();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;

		if (actual == nullptr)
			cout << "FAIL - nullptr" << endl;
		else if (*actual != expected)
			cout << "FAIL - differnt value" << endl;
		else if (*actual == expected)
			cout << "PASS - same value" << endl;
	}

	{
		cout << endl << "Test 2 - add two items" << endl << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushFront(3);

		int expected = 3;
		int* actual = testList.GetFront();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;

		if (actual == nullptr)
			cout << "FAIL - nullptr" << endl;
		else if (*actual != expected)
			cout << "FAIL - differnt value" << endl;
		else if (*actual == expected)
			cout << "PASS - same value" << endl;
	}
}

void Tester::Test_GetBack()
{
	DrawLine();
	cout << "TEST: Test_GetBack" << endl;

	{
		cout << endl << "Test 1 - add one item" << endl << endl;
		List<int> testList;

		testList.PushFront(1);

		int expected = 1;
		int* actual = testList.GetBack();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;

		if (actual == nullptr)
			cout << "FAIL - nullptr" << endl;
		else if (*actual != expected)
			cout << "FAIL - differnt value" << endl;
		else if (*actual == expected)
			cout << "PASS - same value" << endl;
	}

	{
		cout << endl << "Test 2 - add two items" << endl << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushBack(3);

		int expected = 3;
		int* actual = testList.GetBack();

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;

		if (actual == nullptr)
			cout << "FAIL - nullptr" << endl;
		else if (*actual != expected)
			cout << "FAIL - differnt value" << endl;
		else if (*actual == expected)
			cout << "PASS - same value" << endl;
	}
}

void Tester::Test_GetCountOf()
{
	DrawLine();
	cout << "TEST: Test_GetCountOf" << endl;

	{
		cout << endl << "Test 1 - empty list" << endl << endl;
		List<int> testList;

		int expected = 0;
		int actual = testList.GetCountOf(1);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - add multiple items of same type" << endl << endl;
		List<int> testList;

		testList.PushBack(1);
		testList.PushBack(1);
		testList.PushBack(1);

		int expected = 3;
		int actual = testList.GetCountOf(1);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl
			<< "Size: " << testList.Size() << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}
}

void Tester::Test_Contains()
{
	DrawLine();
	cout << "TEST: Test_Contains" << endl;

	{
		cout << endl << "Test 1" << endl << endl;
		List<int> testList;

		bool expected = false;
		bool actual = testList.Contains(1);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - add 2 differnt values" << endl << endl;
		List<int> testList;

		testList.PushBack(1);
		testList.PushBack(3);

		bool expected = true;
		bool actual = testList.Contains(1);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}
}

void Tester::Test_RemoveItem()
{
	DrawLine();
	cout << "TEST: Test_Remove" << endl;

	{
		cout << endl << "Test 1 - empty list" << endl << endl;
		List<int> testList;

		bool expected = false;
		bool actual = testList.RemoveItem(1);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 2 - add multiple items then check size" << endl << endl;
		List<int> testList;
		testList.PushBack(3);
		testList.PushBack(3);
		testList.PushBack(1);
		testList.RemoveItem(3);

		bool expected = false;
		bool actual = testList.Contains(3);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << actual << endl;

		if (expected == actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}
}

void Tester::Test_Insert()
{
	DrawLine();
	cout << "TEST: Test_Insert" << endl;

	{
		cout << endl << "Test 1 - insert 1 item" << endl << endl;
		List<int> testList;
		for (int i = 0; i < 2; i++)
			testList.PushBack(3);
		testList.Insert(0, 1);

		int expected = 1;
		int* actual = testList.Get(0);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << endl;

		if (expected == *actual)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << endl << "Test 1 - insert 2 items" << endl << endl;
		List<int> testList;
		for (int i = 0; i < 2; i++)
			testList.PushBack(3);
		testList.Insert(0, 1);
		testList.Insert(2, 1);

		int expected = 1;
		int* actual = testList.Get(0);
		int* actual2 = testList.Get(2);

		cout << "Expected output: " << expected << endl
			<< "Actual output: " << *actual << " and " << *actual2 << endl;

		if (expected == *actual && expected == *actual2)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}
}
