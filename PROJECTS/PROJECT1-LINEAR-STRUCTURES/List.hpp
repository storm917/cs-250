#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;
	
template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount;
	T m_arr[ARRAY_SIZE];

    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
		if (atIndex < 0 || atIndex > m_itemCount)
			return false;

		for (int i = m_itemCount; i >= atIndex; i--)
			m_arr[i] = m_arr[i - 1];

		return true;
    }

    bool ShiftLeft( int atIndex )
    {
		if (atIndex < 0 || atIndex > m_itemCount)
			return false;

		for (int i = atIndex + 1; i < m_itemCount; i++)
			m_arr[i] = m_arr[i + 1];

		return true;
    }

public:
    List()
    {
		m_itemCount = 0;
    }

    ~List()
    {
    }

    // Core functionality
    int     Size() const
    {
		return m_itemCount;
    }

    bool    IsEmpty() const
    {
		if (m_itemCount == 0)
			return true;
		else
			return false;
    }

    bool    IsFull() const
    {
		if (m_itemCount == ARRAY_SIZE)
			return true;
		else
			return false;
    }

    bool    PushFront( const T& newItem )
    {
		if (IsFull())
			return false;

		ShiftRight(0);
		m_arr[0] = newItem;
		m_itemCount++;
		return true;
    }

    bool    PushBack( const T& newItem )
    {
		if (IsFull())
			return false;

		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true;
    }

    bool    Insert( int atIndex, const T& item )
    {
		if (atIndex < 0 || atIndex >= m_itemCount)
			return false;

		ShiftRight(atIndex);
		m_arr[atIndex] = item;
		m_itemCount++;
		return true;
    }

    bool    PopFront()
    {
		if (IsEmpty())
			return false;

		ShiftLeft(0);
		m_itemCount--;
		return true;
    }

    bool    PopBack()
    {
		if (IsEmpty())
			return false;

		m_itemCount--;
		return true;
    }

    bool    RemoveItem( const T& item )
    {
		if (IsEmpty())
			return false;

		int removeAtIndex[ARRAY_SIZE];
		int j = 0;

		for (int i = 0; i < Size(); i++)
		{
			if (m_arr[i] == item)
			{
				removeAtIndex[j] = i;
				j++;
			}
		}

		for (int i = 0; i < j; i++)
		{
			RemoveIndex(removeAtIndex[i]);
		}

		return true;
    }

    bool    RemoveIndex( int atIndex )
    {
		if (IsEmpty())
			return false;
		
		if (atIndex < 0 || atIndex >= m_itemCount)
			return false;

		ShiftLeft(atIndex);
		m_itemCount--;
		return true;
    }

    void    Clear()
    {
		m_itemCount = 0;
    }

    // Accessors
    T*      Get( int atIndex )
    {
		if (atIndex < 0 || atIndex >= m_itemCount)
		{
			return nullptr;
		}

		return &m_arr[atIndex];
    }

    T*      GetFront()
    {
		return &m_arr[0];
    }

    T*      GetBack()
    {
		return &m_arr[m_itemCount - 1];
    }

    // Additional functionality
    int     GetCountOf( const T& item ) const
    {
		int j = 0;

		for (int i = 0; i < Size(); i++)
		{
			if (m_arr[i] == item)
			{
				j++;
			}
		}

		return j;
    }

    bool    Contains( const T& item ) const
    {
		return (GetCountOf(item) > 0);
    }

	void Display()
	{
		cout << "\t List size: " << Size() << endl;
		for (int i = 0; i < Size(); i++)
		{
			T* item = Get(i);

			cout << "\t " << i << " = ";

			if (item == nullptr)
			{
				cout << "nullptr" << endl;
			}
			else
			{
				cout << *item << endl;
			}
		}
	}

    friend class Tester;
};


#endif
